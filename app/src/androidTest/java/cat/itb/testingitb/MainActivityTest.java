package cat.itb.testingitb;

import androidx.test.espresso.action.ViewActions;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.schibsted.spain.barista.rule.BaristaRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isClickable;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static com.schibsted.spain.barista.assertion.BaristaVisibilityAssertions.assertDisplayed;
import static com.schibsted.spain.barista.interaction.BaristaClickInteractions.clickOn;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {
    private final String USER_TO_BE_TYPED = "admin";
    private final String PASS_TO_BE_TYPED = "admin";
    @Rule
    public ActivityScenarioRule<MainActivity> activityScenarioRule = new ActivityScenarioRule<>(MainActivity.class);
    @Rule
    public BaristaRule<MainActivity> baristaRule = BaristaRule.create(MainActivity.class);


    @Test
    public void elements_on_MainActivity_are_displayed_correctly() {
        onView(withId(R.id.textView)).check(matches(isDisplayed()));
        onView(withId(R.id.button)).check(matches(isDisplayed()));

    }


    @Test
    public void text_on_MainActivty_are_correct() {
        onView(withId(R.id.textView)).check(matches(withText(R.string.main_activity_title)));
        onView(withId(R.id.button)).check(matches(withText(R.string.next)));
    }


    @Test
    public void button_is_clickable_and_text_button_changes_to_Back() {
        onView(withId(R.id.button)).check(matches(isClickable()));
        onView(withId(R.id.button)).perform(click()).check(matches(withText("Back")));
    }


    @Test
    public void login_form_behaviour() {
        onView(withId(R.id.userName)).perform(typeText(USER_TO_BE_TYPED), ViewActions.closeSoftKeyboard());
        onView(withId(R.id.password)).perform(typeText(PASS_TO_BE_TYPED), ViewActions.closeSoftKeyboard());
        onView(withId(R.id.button)).perform(click()).check(matches(withText("Logged")));
    }

    @Test
    public void next_buttton_click_go_to_SecondActivity() {
        //Espresso
        onView(withId(R.id.button)).perform(click());
        onView(withId(R.id.secondActivity)).check(matches(isDisplayed()));
    }

    @Test
    public void next_buttton_click_go_to_SecondActivity_with_Barista() {
        //Barista
        clickOn(R.id.button);
        assertDisplayed(R.id.secondActivity);
    }

    @Test
    public void go_to_SecondActivity_and_back_to_MainActivity() {
        next_buttton_click_go_to_SecondActivity();
        onView(withId(R.id.buttonSecond)).perform(click());
        onView(withId(R.id.mainActivity)).check(matches(isDisplayed()));
        next_buttton_click_go_to_SecondActivity();
        pressBack();
    }

    @Test
    public void test_app() {
        onView(withId(R.id.userName)).perform(typeText(USER_TO_BE_TYPED), ViewActions.closeSoftKeyboard());
        onView(withId(R.id.password)).perform(typeText(PASS_TO_BE_TYPED), ViewActions.closeSoftKeyboard());
        onView(withId(R.id.button)).perform(click());
        onView(withId(R.id.secondActivity)).check(matches(isDisplayed()));
        onView(withId(R.id.textViewSecond)).check(matches(withText("Welcome back " + USER_TO_BE_TYPED)));
        onView(withId(R.id.buttonSecond)).perform(click());
        onView(withId(R.id.mainActivity)).check(matches(isDisplayed()));
        onView(withId(R.id.userName)).check(matches(withText("")));
        onView(withId(R.id.password)).check(matches(withText("")));

    }


}
